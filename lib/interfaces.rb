#
# rm-srri/lib/interfaces.rb
# vr 0.8.0
require_relative 'interface/iDisposable.rb'
require_relative 'interface/iRenderable.rb'
require_relative 'interface/iZOrder.rb'
