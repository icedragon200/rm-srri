#
# src/module/modules.rb
#

# core
require_relative 'module/Kernel.rb'

# main
require_relative 'module/Audio.rb'
require_relative 'module/Graphics.rb'
require_relative 'module/Input.rb'

require_relative 'module/RPG.rb'

# Extended
require_relative 'module/TextureTool.rb'


